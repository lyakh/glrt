LDSCRIPT := STM32L476RGTx_FLASH.ld
TARGET := test
CC := arm-none-eabi-gcc
CFLAGS = -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -Os -Wall -fdata-sections -ffunction-sections -std=gnu11 -MMD -MP -MF"$(@:%.o=%.d)"
LDFLAGS := -mcpu=cortex-m4 -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -T$(LDSCRIPT) --specs=nosys.specs -Wl,-Map=$(TARGET).map -Wl,--gc-sections -static --specs=nano.specs -Wl,--start-group -Wl,--end-group
OBJECTS := main.o

all: $(TARGET).elf

%.o: %.c Makefile
	$(CC) -c $(CFLAGS) -Wa,-a,-ad,-alms=$(notdir $(<:.c=.lst)) $< -o $@

$(TARGET).elf: $(OBJECTS) Makefile
	$(CC) -o $@ $(OBJECTS) $(LDFLAGS)

clean:
	rm -f *.elf *.o *.map *.d *.lst

.PHONY: all clean
